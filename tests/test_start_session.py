from client import Client


def test_start_session():
    client = Client(["l", "r", "u", "d"], {"l": "r", "r": "l", "u": "d", "d": "u", None: None})
    read_file = open("read_file.txt", "r")
    write_file = open("write_file.txt", "w")
    client.start_session(read_file, write_file)
    assert client.game_ended
