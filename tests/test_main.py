import pytest

from client import main


def test_main_errors():
    with pytest.raises(ConnectionRefusedError):
        assert main() == "[Errno 61] Connection refused"
