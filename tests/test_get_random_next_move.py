from client import get_random_move


def test_get_random_next_move():
    assert get_random_move(["l", "r", "u", "d"], "l") != "l"
