from client import Client


def test_client_check_responcesand_get_next_move():
    client = Client(["l", "r", "u", "d"], {"l": "r", "r": "l", "u": "d", "d": "u", None: None})
    client.check_responces_and_get_next_move("r")
    assert client.cmd[-1] in ("r", "l", "u", "d")


def test_client_start_passed():
    client = Client(["l", "r", "u", "d"], {"l": "r", "r": "l", "u": "d", "d": "u", None: None})
    client.start_passed()
    assert client.cmd == "start"
    assert not client.is_first_command
