import random
import re
import socket
from typing import List, Dict

HOST = "127.0.0.1"
PORT = 8080


def send_request_and_get_responce(request: str, write_file, read_file) -> str:
    write_file.write(f"{request}\n")
    write_file.flush()
    res = read_file.readline().strip()
    print(res)
    return res


def get_random_move(moves: List[str], move_before: str):
    return random.choice([move for move in moves if move != move_before])


class Client:
    def __init__(self, moves: List[str], reverse_moves: Dict):
        self.is_first_command = True
        self.next_move = None
        self.is_escaping = False
        self.game_ended = False
        self.moves = moves
        self.reverse_moves = reverse_moves
        self.cmd = None

    def check_responces_and_get_next_move(self, data: str) -> None:
        if re.search(r"Вы спасены", data):
            self.game_ended = True
        if re.search(r"Вы активировали последнюю кнопку, двери открыты, БЕГИ", data):
            self.is_escaping = True
        if re.search(r"Тупик!", data):
            self.next_move = get_random_move(self.moves, self.next_move)
        else:
            self.next_move = get_random_move(self.moves, self.reverse_moves[self.next_move])

        self.cmd = f"move {self.next_move}"

    def start_passed(self):
        self.cmd = "start"
        self.is_first_command = False

    def start_session(self, read_file, write_file):
        while not self.game_ended:
            data = read_file.readline().strip()

            while data:
                self.check_responces_and_get_next_move(data)
                data = read_file.readline().strip()

            if not self.is_first_command and not self.is_escaping:
                data = send_request_and_get_responce("a", write_file, read_file)
                while data:
                    data = read_file.readline().strip()
            else:
                self.start_passed()

            write_file.write(f"{self.cmd}\n")
            write_file.flush()


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        read_file = s.makefile(mode="r", encoding="utf-8")
        write_file = s.makefile(mode="w", encoding="utf-8")

        Client(moves=["l", "r", "u", "d"],
               reverse_moves={"l": "r", "r": "l", "u": "d", "d": "u", None: None}).start_session(read_file, write_file)


if __name__ == "__main__":
    main()
